package com.thpa.non.TestApp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class Intro_ToHome extends AppCompatActivity {

    //set interval time
    private static int splashInterval = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro__to_home);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Todo Auto-generate method stub
                Intent i = new Intent(Intro_ToHome.this,MainActivity.class);
                startActivity(i);
                this.finish();

            }
            private  void finish(){
                //Todo Auto-generate method stub

            }
        },splashInterval);

    }
}
