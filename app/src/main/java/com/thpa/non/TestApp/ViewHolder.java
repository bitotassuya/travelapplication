package com.thpa.non.TestApp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class ViewHolder extends RecyclerView.ViewHolder {

    View mView;

    public ViewHolder(View itemView) {
        super(itemView);
        mView = itemView;

        //set Item click
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListenner.onItemClick(view, getAdapterPosition());
            }
        });

        itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mClickListenner.onItemLongClick(view, getAdapterPosition());
                return true;
            }
        });

    }

    //Set detail to recycler view
    public void setDetail(Context context, String title, String description, String image) {
        //views
        TextView mTitle = mView.findViewById(R.id.txt_title);
        TextView mDescription = mView.findViewById(R.id.txt_description);
        ImageView mImage = mView.findViewById(R.id.img_travel);

        //set data to view

        mTitle.setText(title);
        mDescription.setText(description);
        Picasso.get().load(image).into(mImage);
        //Picasso.with(context).load(image).into(mImage);
    }

    private ViewHolder.ClickListenner mClickListenner;

    //Interface to send callback
    public interface ClickListenner {

        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

    public void setOnClickListener(ViewHolder.ClickListenner clickListenner) {
        mClickListenner = clickListenner;
    }

}

