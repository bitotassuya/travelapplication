package com.thpa.non.TestApp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class StartScreen extends AppCompatActivity {

     Button btnMap,btnList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_screen);

        btnMap = (Button)findViewById(R.id.catacory1);
        btnList = (Button) findViewById(R.id.catacory2);

    }
    //Onclick to MapActivity
    public void CataGory1(View view){
        Toast.makeText(this,"ยินดีต้อนรับสู่ Google Map", Toast.LENGTH_LONG).show();
        Intent clkToMap = new Intent(StartScreen.this,MapsActivity.class);
        startActivity(clkToMap);
    }
    //Onclick to Firabase Recyclerview
    public void CataGory2(View view){
        Toast.makeText(this,"ยินดีต้อนรับสู่รายการสถานที่ท่องเที่ยว",Toast.LENGTH_LONG).show();
        Intent clkFirebase = new Intent(StartScreen.this,PostsListActivity.class);
        startActivity(clkFirebase);
    }
    //Detect Back Button
    @Override
    public void onBackPressed() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(StartScreen.this);

        alertDialog.setTitle("คุณต้องการจะออกจากระบบใช่หรือไม่");

        alertDialog.setPositiveButton("ใช่",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        //คลิกใช่ ออกจากโปรแกรม
                        finish();
                        StartScreen.super.onBackPressed();

                    }
                });

        alertDialog.setNegativeButton("ไม่",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,	int which) {
                        //คลิกไม่ cancel dialog
                        dialog.cancel();
                    }
                });

        alertDialog.show();

    }

}
