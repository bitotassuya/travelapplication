package com.thpa.non.TestApp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.io.ByteArrayOutputStream;

public class PostsListActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    FirebaseDatabase mFirebaseDatabase;
    DatabaseReference mRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts_list);

        //set action bar
        ActionBar actionBar = getSupportActionBar();

        //set title
        actionBar.setTitle("Show List");

        //RecyclerView
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);

        //set Layout
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //send Query toFireBase
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mRef = mFirebaseDatabase.getReference("traver");
    }
    //search data

    private void firebaseSearch(String searchText) {
        Query firebaseSearchQuery = mRef.orderByChild("title").startAt(searchText).endAt(searchText + "\uf8ff");
        FirebaseRecyclerAdapter<Model, ViewHolder> firebaseRecyclerAdapter =
                new FirebaseRecyclerAdapter<Model, ViewHolder>(
                        Model.class,
                        R.layout.cardview_model,
                        ViewHolder.class,
                        firebaseSearchQuery) {

                    @Override
                    protected void populateViewHolder(ViewHolder viewHolder, Model model, int position) {
                        viewHolder.setDetail(getApplicationContext(), model.getTitle(), model.getDescription(), model.getImage());
                    }

                    @Override
                    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                        ViewHolder viewHolder = super.onCreateViewHolder(parent, viewType);

                        viewHolder.setOnClickListener(new ViewHolder.ClickListenner() {
                            @Override
                            public void onItemClick(View view, int position) {
                                //Views
                                TextView txtTitle = view.findViewById(R.id.txt_title);
                                TextView txtDescription = view.findViewById(R.id.txt_description);
                                ImageView imgDetail = view.findViewById(R.id.img_travel);

                                //get data form view
                                String mTitle = txtTitle.getText().toString();
                                String mDecs = txtDescription.getText().toString();
                                Drawable mDrawable = imgDetail.getDrawable();
                                Bitmap mBitmap = ((BitmapDrawable) mDrawable).getBitmap();

                                //pass data to next Activity
                                Intent intent = new Intent(view.getContext(), PostDetailActivity.class);
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                byte[] bytes = stream.toByteArray();
                                mBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);


                                intent.putExtra("image", bytes); //put bitmap image as array to byte
                                intent.putExtra("title", mTitle); //put title
                                intent.putExtra("description", mDecs); //put Description
                                startActivity(intent); //start activity
                            }

                            @Override
                            public void onItemLongClick(View view, int position) {
                                //TODO do your own implemention on long item click

                            }
                        });
                        return viewHolder;
                    }
                };

        //set Adapter to recyclerview
        mRecyclerView.setAdapter(firebaseRecyclerAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //interflate this menu add items to the actonbar if it parent
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                firebaseSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Filter your type
                firebaseSearch(newText);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        //handle other action bar item click here
        if (id == R.id.action_setting) {
            //TODO
            return true;
        }

        return super.onOptionsItemSelected(item);

    }


    //Load data into recyclerview
    @Override
    protected void onStart() {
        super.onStart();
        FirebaseRecyclerAdapter<Model, ViewHolder> firebaseRecyclerAdapter =
                new FirebaseRecyclerAdapter<Model, ViewHolder>(
                        Model.class,
                        R.layout.cardview_model,
                        ViewHolder.class,
                        mRef
                ) {
                    @Override
                    protected void populateViewHolder(ViewHolder viewHolder, Model model, int position) {
                        viewHolder.setDetail(getApplicationContext(), model.getTitle(), model.getDescription(), model.getImage());
                    }
                    @Override
                    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                        ViewHolder viewHolder = super.onCreateViewHolder(parent, viewType);

                        viewHolder.setOnClickListener(new ViewHolder.ClickListenner() {
                            @Override
                            public void onItemClick(View view, int position) {
                                //Views
                                TextView txtTitle = view.findViewById(R.id.txt_title);
                                TextView txtDescription = view.findViewById(R.id.txt_description);
                                ImageView imgDetail = view.findViewById(R.id.img_travel);

                                //get data form view
                                String mTitle = txtTitle.getText().toString();
                                String mDecs = txtDescription.getText().toString();
                                Drawable mDrawable = imgDetail.getDrawable();
                                Bitmap mBitmap = ((BitmapDrawable) mDrawable).getBitmap();

                                //pass data to next Activity
                                Intent intent = new Intent(view.getContext(), PostDetailActivity.class);
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                byte[] bytes = stream.toByteArray();
                                mBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);


                                intent.putExtra("image", bytes); //put bitmap image as array to byte
                                intent.putExtra("title", mTitle); //put title
                                intent.putExtra("description", mDecs); //put Description

                                startActivity(intent); //start activity
                            }

                            @Override
                            public void onItemLongClick(View view, int position) {
                                //TODO do your own implemention on long item click

                            }
                        });
                        return viewHolder;
                    }
                };
        //set Adapter to recyclerview
        mRecyclerView.setAdapter(firebaseRecyclerAdapter);
    }
}
