package com.thpa.non.TestApp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class PostDetailActivity extends AppCompatActivity {
    TextView titileTravel,descriptionTravel;
    ImageView imgTravel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);
        //set Title
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Travel Detail");
        //set show Title
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        //initail Value
        titileTravel =  findViewById(R.id.txt_detail_title);
        descriptionTravel =  findViewById(R.id.txt_detail_description);
        imgTravel =  findViewById(R.id.img_detail_travel);

        //get data from intent
        byte[] bytes = getIntent().getByteArrayExtra("image");
        String title= getIntent().getStringExtra("title");
        String description = getIntent().getStringExtra("description");
        Bitmap bmp = BitmapFactory.decodeByteArray(bytes,0,bytes.length);

        //set data to View
        titileTravel.setText(title);
        descriptionTravel.setText(description);
        imgTravel.setImageBitmap(bmp);
    }
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return true;
    }
}
